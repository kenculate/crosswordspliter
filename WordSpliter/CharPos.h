#ifndef CHARPOS_H
#define CHARPOS_H

#pragma once
class CharPos
{
	friend class Word;
public:
	CharPos();
	CharPos(int x, int y);
	~CharPos();
	int x;
	int y;
private:

};


#endif // !CHARPOS_H