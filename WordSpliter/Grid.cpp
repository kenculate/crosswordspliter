#include "pch.h"
#include "Grid.h"
#include <iostream>

Grid::Grid(){}

Grid::Grid(size_t width, size_t height, size_t cellSize)
{
	this->width = width;
	this->height = height;

	this->cellCount = width * height;
	this->totalSize = width * height * cellSize;
	this->cellSize = cellSize;
	std::cout << endl << cellCount << endl;
	int c = 0;
	for (size_t i = 0; i < width; i++)
	{
		for (size_t j = 0; j < height; j++)
		{
			cells.push_back(Cell(i * cellSize, j * cellSize, cellSize, c));
			c++;
		}
	}
}

Grid::~Grid(){}

void Grid::putWord(Word *word, int x, int y, Orient orient)
{
	word->setXY(x, y);
	for (size_t i = 0; i < word->getSize(); i++)
	{
		if (orient == Orient::horizontal)
		{
			if (x + i <= width) {
				int idx = getIndex(x + i, y);
				cells[idx].value = word->getChar(i);
			}
		}
		else {
			if (x + i <= height) {
				int idx = getIndex(x, y + i);
				cells[idx].value = word->getChar(i);
			}
		}
	}
}

void Grid::Render()
{
	for (size_t i = 0; i < cellCount; i++)
	{
		cells[i].render();
		if ((i+1) % width == 0)
			std::cout << endl;
	}
}
