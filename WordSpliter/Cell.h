#pragma once
class Cell
{
	friend class Grid;
public:
	Cell();
	Cell(int x, int y, size_t size, int index);
	~Cell();

	void render();
	int x, y;
	size_t size;
	int index;
	char value = ' ';
};

