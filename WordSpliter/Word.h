#ifndef  WORD_H
#define WORD_H
#include <algorithm>
#include <iostream>	
#include <string>
#include <stdlib.h>
#include <vector>
#include "CharPos.h"
using namespace std;
#pragma once
enum Orient
{
	horizontal, vertical
};
class Word
{
	
public:
	class Cross
	{
	public:
		int crossingIndex, crossedIndex;
		int index;
		char ch;
		bool checked;
		string wordStr;
		bool getChecked() { 
			//std::cout << "getting " << this << " crossing " << crossingIndex << " crossed " << crossedIndex << " checked " << checked << endl; 
			return checked; 
		}
		void setChecked(bool value) { checked = value; }
	};

	Word();
	~Word();
	Word(string _word);
	char getChar(int _index);
	void render();
	size_t getSize() { return size; }
	void addCost(int _cost) { cost += _cost; }
	int getCost() { return cost; }
	string toString() { return word; };
	Orient getOrient() { return orient; }
	vector<int> getPlaces() { return places; }
	int getX() { return x; }
	int getY() { return y; }
	void setX(int _x) { x = _x; setRange(); }
	void setY(int _y) { y = _y; setRange();
	}
	void setXY(int _x, int _y) { x = _x; y = _y; setRange();}
	bool getPlaced() { return placed; }
	void addPlaces(int i) { 
		places.push_back(i); 
	}
	struct Rect
	{
		int left, top, right, bottom;
		Rect() {}

		Rect(int _minX, int _minY, int _maxX, int _maxY) {
			left = _minX;
			top = _minY;
			right = _maxX;
			bottom = _maxY;
		}
		bool isInRange(Rect r) {
			if ((left < r.right && right > r.left) &&
				(top < r.bottom && bottom > r.top)) {
				return true;
			}
			return false;
		}

	};


	void setRange() {
		rect.left = getX();
		rect.top = getY();
		rect.right = getX() + (orient == Orient::horizontal ? getSize() : 1);
		rect.bottom = getY() + (orient == Orient::horizontal ? 1 : getSize());
	}

	

	vector<Cross*> getCrosses() { return crosses; }
	void addCross(int i, int j, int index, char _ch, string str) {
		
		Cross *c = new Cross();
		c->crossingIndex = i;
		c->crossedIndex = j;
		c->index = index;
		c->ch = _ch;
		c->wordStr = str;
		crosses.push_back(c);
	}

	bool checkOverlap(Word *crossedWord, vector<Word*> words) {
		for (size_t wi = 0; wi < words.size(); wi++)
		{
			if (words[wi] != crossedWord && crossedWord->placed && words[wi]->placed) {
				if (rect.isInRange(words[wi]->rect)) {
					return true;
				}
			}
		}
		return false;
	}

	bool place(int i, int j, Word *crossedWord, int crossIndex, vector<Word*> words) {
		if (std::find(places.begin(), places.end(), i) != places.end())
			return false;
		vector<int> t = {};
		if (crossedWord) {
			//cout << "placing " << toString() << " at " << crossedWord->toString()
			//	<< " at " << j << " places " << crossedWord->getPlaces().size() << endl;

			if (crossedWord->getPlaces().size() >= crossedWord->getSize() / 3)
				return false;
			if (crossedWord->getPlaces().size() > 0) {
				for (size_t pi = 0; pi < crossedWord->getPlaces().size(); pi++)
				{
					if (crossedWord->getPlaces()[pi] == j)
						return false;
				}
			}
			if (crossIndex >= 0 && crossIndex < crosses.size()) {
				if (crosses[crossIndex]->getChecked()) {
					return false;
				}
			}
			if (crossedWord->getOrient() == Orient::horizontal) {
				orient = Orient::vertical;
				setX(crossedWord->getX() + j);
				setY(crossedWord->getY() - i);
			}
			else {
				orient = Orient::horizontal;
				setX(crossedWord->getX() - i);
				setY(crossedWord->getY() + j);
			}
			if (checkOverlap(crossedWord, words))
				return false;
			crossedWord->addPlaces(j);
			addPlaces(i);
			cout << "placing " << toString() << (orient == 0 ? " H " : " V ") << " & " 
				<< crossedWord->toString() << (crossedWord->getOrient() == 0 ? " H " : " V ")
				<< " at " << i << ":"  << j << endl << endl;
		}
		else {
			placed = true;
			return true;
		}
		placed = true;
		return true;

	}

	bool operator==(Word *other) {
		return word == other->toString();
	}
	bool operator!=(Word *other) {
		return word != other->toString();
	}
	bool operator < (Word *other) {
		return getCost() < other->getCost();
	}
	bool operator > (Word *other) {
		return getCost() > other->getCost();
	}

private:
	size_t size;
	int x, y = 0;
	vector<char> chars;
	string word = "";
	Orient orient;
	int cost;
	vector<Cross*> crosses;
	vector<int> places = {};
	bool placed = false;
	Rect rect;
};

#endif // ! Word_H
