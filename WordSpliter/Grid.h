#ifndef GRID_H
#define	GRID_H
#include <vector>
#include "Cell.h"
#include "Word.h"
using namespace std;
#pragma once
class Grid
{
public:
	Grid();
	Grid(size_t width, size_t height, size_t cellSize);
	~Grid();
	void putWord(Word *word, int x, int y, Orient orient);
	void Render();
	int getIndex(int x, int y) { return (y * height + x); }

	size_t cellCount;
	size_t cellSize;
	size_t width, height;
	size_t totalSize;
	vector<Cell> cells;
};

#endif // !GRID_H
