#include "pch.h"
#include "Spliter.h"
#include <algorithm>
using namespace std;

Spliter::Spliter()
{
}


Spliter::Spliter(vector<Word*> _words, Grid *grid)
{
	this->grid = grid;
	//for (size_t i = 0; i < _words.size(); i++)
	//{
	//	words.push_back(_words[i]);
	//}
	words = _words;
	splitWords();
	arrangeWords();
}

Spliter::~Spliter()
{
}


void Spliter::splitWords()
{
	for (size_t i = 0; i < words.size(); i++)
	{
		calcWordCost(words[i]);
	}
	//sortWords();
	printWords();
	words[0]->place(0, 0, nullptr, -1, {});
	for (size_t i = 1; i < words.size(); i++)
	{
		for (size_t j = 0; j < words[i]->getCrosses().size(); j++)
		{
			bool placed = false;
			cout << "checking " << words[i]->toString() << " with " << words[i]->getCrosses()[j]->wordStr << endl;
			if (i != words[i]->getCrosses()[j]->index) {
				if (words[i]->place(
					words[i]->getCrosses()[j]->crossingIndex,
					words[i]->getCrosses()[j]->crossedIndex,
					words[words[i]->getCrosses()[j]->index],
					j, words
				)) {
					words[i]->getCrosses()[j]->setChecked(true);
					placed = true;
					break;
				}
			}
			if (placed)
				break;
		}
	}
}


void Spliter::arrangeWords()
{
	int minX = 999;
	int minY = 999;
	for (size_t i = 0; i < words.size(); i++)
	{
		minX = std::min(words[i]->getX(), minX);
		minY = std::min(words[i]->getY(), minY);
	}
	for (size_t i = 0; i < words.size(); i++)
	{
		if (words[i]->getPlaced()) {
			words[i]->setX(words[i]->getX() + abs(minX));
			words[i]->setY(words[i]->getY() + abs(minY));
			grid->putWord(words[i], words[i]->getX(), words[i]->getY(), words[i]->getOrient());
		}
		else {
			cout << words[i]->toString() << " does not match any" << endl;
		}
	}
}

void Spliter::calcWordCost(Word* word) {
	for (size_t i = 0; i < words.size(); i++)
	{
		if (word != words[i]) {
			int c = 0;
			for (size_t j = 0; j < word->getSize(); j++)
			{
				for (size_t k = 0; k < words[i]->getSize(); k++)
				{
					if (word->getChar(j) == words[i]->getChar(k)) {
						word->addCost(1);
						word->addCross(j, k, i, words[i]->getChar(k), words[i]->toString());
						c++;
					}
				}
			}
		}
	}
}

void Spliter::printWords() {
 	for (size_t i = 0; i < words.size(); i++)
	{
		cout << words[i]->toString() << " " << words[i]->getCost() << endl;
	}
}
void Spliter::sortWords() {
	std::sort(words.begin(), words.end(), assendingSort);
}
bool assendingSort(Word *w1, Word *w2) {
	return w1 > w2;
}
