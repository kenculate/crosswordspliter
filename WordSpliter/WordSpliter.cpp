// WordSpliter.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include "pch.h"
#include <iostream>
#include "Word.h"
#include "Grid.h"
#include "Spliter.h"
int main()
{
	bool wordsEntered = false;
	vector<Word*> words;
	while (!wordsEntered)
	{
		string word = "";
		cout << "enter a word ";
		std::getline(cin, word);
		bool entered = false;
		if (!word.empty()) {
			words.push_back(new Word(word));
		}
		else {
			if (words.size() == 0) {
				words.push_back(new Word("backslash"));
				words.push_back(new Word("backspace"));
				words.push_back(new Word("insertxrxxrx"));
				words.push_back(new Word("home"));
				words.push_back(new Word("pagedown"));
			}
			wordsEntered = true;
		}
	}


	//words.push_back(new Word("abcd"));
	//words.push_back(new Word("ebgf"));
	//words.push_back(new Word("hifk"));

	if (words.size() > 0) {
		Grid* grid = new Grid(20, 20, 1);
		Spliter spliter = Spliter(words, grid);
		words.push_back(new Word("pagedown"));
		cout << endl << endl;
		grid->Render();
	}
	cout << "DONE";
	std::getchar();
}
