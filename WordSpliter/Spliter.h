#ifndef SPLITER_H
#define SPLITER_H

#include "Grid.h"

#pragma once
class Spliter
{
public:
	Spliter();
	Spliter(vector<Word*> words, Grid* grid);
	~Spliter();
	void arrangeWords();
	void splitWords();
	void printWords();
	void calcWordCost(Word* word);
	void sortWords();

private:
	Grid *grid;
	vector<Word*> words;
};
bool assendingSort(Word *w1, Word *w2);

#endif // !SPLITER_H
